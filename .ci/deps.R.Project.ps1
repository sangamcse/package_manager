Set-StrictMode -Version latest

# https://github.com/AdmiringWorm/chocolatey-packages/issues/70
function Add-R-to-PATH {
    $list = Get-ChildItem -Directory 'C:\Program Files\R' |
        Out-String

    Write-Verbose $list

    Get-ChildItem -Directory 'C:\Program Files\R' |
        ForEach-Object {
            $R_ROOT = $_.FullName

            Write-Host 'Setting R_ROOT='$R_ROOT

            $env:R_ROOT = $R_ROOT
            Set-ItemProperty -Path 'HKCU:\Environment' -Name 'R_ROOT' -Value $R_ROOT

            $R_BIN = ($R_ROOT + '\bin')

            Install-ChocolateyPath -PathToInstall $R_BIN
        }

    if (!$R_ROOT) {
        throw ('R not found')
    }
}

function Initialize-CRAN {
    $cran_config = "
local({r <- getOption('repos')
       r['CRAN'] <- 'http://cran.r-project.org'
       options(repos=r)})
"
    Set-Content "$env:R_ROOT\etc\Rprofile.site" $cran_config

    $BINPREF = $env:BINPREF -replace '\\', '/'
    $env:BINPREF = $BINPREF
    Set-ItemProperty -Path 'HKCU:\Environment' -Name 'BINPREF' -Value $BINPREF
}

function Complete-Install {
    Add-R-to-PATH
    Initialize-CRAN
}

Export-ModuleMember -Function Add-R-to-PATH, Complete-Install
